package main

import (
	"bufio"
	"errors"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

const baseURL = "https://www.bbc.co.uk"
const linkFilename = "links.txt"

var linkregex *regexp.Regexp
var url string

func init() {
	flag.StringVar(&url, "url", "", "The url to the page of an episode for which to save all the series' video links. Must start with "+baseURL+", and should not contain query paramters.")
	flag.Parse()
	r, _ := regexp.Compile(`href=\"(/iplayer/episode/\w+/[^/\"]*)\"`)
	linkregex = r
}

func main() {
	if url == "" {
		log.Fatal("Parameter 'url' cannot be empty.")
	}
	if !strings.HasPrefix(url, baseURL) {
		log.Fatal("Parameter 'url' must starts with " + baseURL)
	}
	if strings.Contains(url, "?") {
		log.Fatal("Parameter 'url' must not contain query parameters.")
	}
	links := make([]string, 0)
	links = append(links, url)
	for url != "" {
		log.Println("Getting url:", url)
		text, err := get(url)
		if err != nil {
			log.Fatal("ERROR getting url", url, ":", err)
		}
		episodes, err := scrape(text, `<ul class="gel-layout related-episodes__list"`, `</ul>`)
		if err != nil {
			log.Fatal("ERROR cannot scrape url", url, ":", err)
		}
		for _, match := range linkregex.FindAllStringSubmatch(episodes, -1) {
			link := baseURL + match[1]
			pos := strings.Index(link, "?")
			if pos >= 0 {
				link = link[:pos]
			}
			links = append(links, link)
		}
		log.Println("Collected", len(links), "up to now.")
		url = ""
		nextLink, err := scrape(text, `aria-label="Next Page" href="`, `"><span`)
		if err == nil {
			log.Println("Found Next Page url.")
			url = baseURL + strings.ReplaceAll(nextLink, "&amp;", "&")
		}
	}
	saveLinks(links)
	log.Println("Done! Saved", len(links), "links.")
}

func get(url string) (string, error) {
	response, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	data, _ := io.ReadAll(response.Body)
	return string(data), nil
}

func scrape(input, from, to string) (string, error) {
	pos := strings.Index(input, from)
	if pos < 0 {
		return "", errors.New("input does not contain from parameter")
	}
	pos += len(from)
	input = input[pos:]
	pos = strings.Index(input, to)
	if pos < 0 {
		return "", errors.New("input does not contain to parameter")
	}
	return input[:pos], nil
}

func saveLinks(links []string) {
	file, _ := os.Create(linkFilename)
	writer := bufio.NewWriter(file)
	defer writer.Flush()
	for _, link := range links {
		writer.WriteString(link + "\n")
	}
}
